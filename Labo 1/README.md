# ArmadaWeb

Site web pour l'armada - Esigelec

Labo 1 : Le dossier de conception
Le but est de créer le dossier de conception, en incluant les éléments suivant :
1. Schéma d’arborescence du site (liste des pages et liens entre les pages),
2. Maquette de la page d’accueil en version mobile (petit  écran) et fixe (écran standard de PC)
3. Description de chaque page (contenu, images, liens)
4. Une première version du diagramme de la base de données
5. Le planning prévisionnel de codage pour chaque membre du binôme.

Utilisez Pencil pour les schémas et les maquettes, et un éditeur UML pour le schéma de la base.
Vous devez surtout "penser" le site avant de le coder.
Ce labo a pour but de vous forcer à réfléchir sur le contenu, les pages, les liens, la navigation, etc.
Une fois le dossier de comception écrit, vous devez avoir cadré le fonctionnement de votre site.