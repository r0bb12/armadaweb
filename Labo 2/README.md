# ArmadaWeb

Site web pour l'armada - Esigelec

Labo 2 : La page d'accueil
Le but est de créer la page d'accueil en HTML et CSS
1. Utiliser un éditeur de text (VS Code, Sublime Text, etc.)
2. Télécharger Bootstrap (à télécharger sur getbootstrap.com)
4. Utiliser la plateforme GitLab.com pour achiver le code et les documents

Il est temps de coder la page d'accueil en HTML.
Grace à Bootstrap, vous aurez une page rapidement et sans trop d'effort, qui est à la fois design et responsive (compatible mobile).
A la fin du labo, vous devrez avoir terminé la structure html de la page, sans le contenu exacte.