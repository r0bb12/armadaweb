<?php
include 'Etape4_Fonctions.inc';
if(!isset($_COOKIE['ArmadaLogin']))
{
header('Location: Etape4.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Etape 4</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <h1>Etape 4</h1>
    <p>Page d'accuil, vous êtes connecté !</p>
    <p>
        <?php
            $user = Armada_GetUtilisateur($_COOKIE['ArmadaLogin']);
            echo 'Bonjour '.$user['Prenom'].' '.$user['Nom'];
        ?>
	</p>
</body>
</html>