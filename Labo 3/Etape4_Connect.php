<?php
if(!isset($_POST['login']) && !isset($_POST['password']))
{
    header('Location: Etape4.php');
}
else
{
    // On va vérifier les variables
    require('Etape4_Fonctions.inc'); // On réclame le fichier

    if (Armada_ConnectUtilisateur($_POST['login'], $_POST['password']))
    {
        // Creation du cookie
        setcookie('ArmadaLogin', $_POST['login'], time() + (86400 * 30), '/', 'labo3.test', false, true);
        // On redirige vers la page suivante
        header('Location: Etape4_Accueil.php');
    }
    else
    {
        // On redirige vers la page login
        header('Location: Etape4.php');
    }
}
?>