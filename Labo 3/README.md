# ArmadaWeb

Site web pour l'armada - Esigelec

Labo 3 : PHP
Le but est de convertir la page d'accueil en PHP
1. Installation de XAMP
2. Convertir la page d'accueil en PHP
3. Créer toutes les pages en php (vide, mais fonctionnelle)

C'est le moment de faire du PHP.
Vous apprendrez à transformer votre page d'accueil HTML en PHP, pour dynamiser les zones qui doivent l'être.
A la fin du labo, vous devrez avoir créé toutes les pages en PHP, vide avec simplement des liens de navigation.